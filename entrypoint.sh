#!/bin/sh

#makes sure that any errors are printed to the screen and fails fast
set -e

# runs the template file through the substitute program to insert the environment variables into the file and save it in the proper location
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# tells nginx to start but not as a background process. This is best practices for docker.
nginx -g 'daemon off;'